package com.noddysoft.wordlyandroid.classes;

import java.util.List;

public class GameSession {
    private Boolean offlineMode;
    private WordList wordlist;
    private Category selectedCategory;
    private List<Category> availableCategories;
    private static GameSession instance;
    private Long countDownTime = Long.valueOf(60000); //default value is 60 seconds


    private GameSession() {
        offlineMode = false;
    }

    public static synchronized GameSession getInstance() {
        if (instance == null) {
            instance = new GameSession();
        }
        return instance;
    }


    public Boolean getOfflineMode() {
        return offlineMode;
    }

    public void setOfflineMode(Boolean offlineMode) {
        this.offlineMode = offlineMode;
    }

    public WordList getWordlist() {
        return wordlist;
    }

    public Category getSelectedCategory() {
        return selectedCategory;
    }

    public void setSelectedCategory(Category selectedCategory) {
        this.selectedCategory = selectedCategory;
    }

    public List<Category> getAvailableCategories() {
        return availableCategories;
    }

    public void setAvailableCategories(List<Category> availableCategories) {
        this.availableCategories = availableCategories;
    }

    public void setCountDownTime(Long countDownTimeInMilliseconds) {
        countDownTime = countDownTimeInMilliseconds;
    }

    public Long getCountDownTimeInMilliseconds() {
        return countDownTime;
    }

    public String getCountDownTimeInSecondsAsString() {
        Long countDownTimeInSeconds = countDownTime / 1000;
        return countDownTimeInSeconds.toString();
    }
}
