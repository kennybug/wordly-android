package com.noddysoft.wordlyandroid.classes;

/**
 * Created by Erik on 03.12.2018.
 */

public class Word {
    Long id;
    String wordtext;
    boolean delflg;

    public Word(){}
    public Word(Long id, String wordtext){
        this.id = id;
        this.wordtext = wordtext;
        this.delflg = false;
    }

    public String getWordtext() {
        return wordtext;
    }

    public long getId() {
        return id;
    }

    public void setWordtext(String wordtext){
        this.wordtext = wordtext;
    }

    public void setId(Long id){
        this.id = id;
    }

    public String toString() {
        return "id: " + id.toString() + ", text: " + wordtext;
    }

    public void setDelflg(boolean delflg) {
        this.delflg = delflg;
    }

    public boolean getDelflg() {
        return delflg;
    }
}
