package com.noddysoft.wordlyandroid.classes;

import android.app.Application;
import android.content.Context;
import android.text.TextUtils;
import android.util.Log;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.JsonRequest;
import com.android.volley.toolbox.Volley;
import com.noddysoft.wordlyandroid.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;


/**
 * Created by Erik on 06.06.2018.
 */

public class WebserviceConnector extends Application {
    private static final String TAG = WebserviceConnector.class.getSimpleName();
    private RequestQueue requestQueue;
    private String url;
    private Context context;
    private static WebserviceConnector instance;

    private WebserviceConnector(Context context, String url) {
        this.context = context;
        this.url = url;
        requestQueue = Volley.newRequestQueue(context);
    }

    //TODO: Speed up by only using synchronized if it was null when checking first time(?)
    public static synchronized WebserviceConnector getInstance(Context context, String url) {
        if (instance == null) {
            instance = new WebserviceConnector(context, url);
        }
        return instance;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public void requestWord(final VolleyCallback volleyCallback, String urlextension) {
        String finalurl = url + urlextension;

        JsonObjectRequest jsonReq = new JsonObjectRequest(Request.Method.GET, finalurl, null,

                //stringrequest argument #3:
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {

                        try {
                            volleyCallback.onSuccessResponse(new Word(response.getLong("id"), response.getString("text")));
                        } catch (Exception e) {
                            e.getMessage();
                        }

                        //Todo: Change so that list of words are retrieved when starting game.

                    }
                },

                //argument #4:
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        System.out.println("error occurred: " + error.toString());
                    }
                });

        requestQueue.add(jsonReq);
    }

    public void requestWordlist(final WordList wordList, String urlextension, final VolleyCallbackWordlist volleyCallbackWordlist) {
        String finalUrl = url + urlextension;

        //the jsonReq object is just a specification for what is to happen when queue.add is called!
        //nothing of the body of jsonReq is called until queue.add.
        JsonArrayRequest jsonArrayReq = new JsonArrayRequest(Request.Method.GET, finalUrl, null,
                //stringrequest argument #3:
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        JSONObject jsonObject;

                        try {
                            for (int i = 0; i < response.length(); i++) {
                                jsonObject = response.getJSONObject(i);
                                Long id = jsonObject.getLong("id");
                                String wordtext = jsonObject.getString("text");
                                wordList.addSingleWord(new Word(id, wordtext));
                            }
                            volleyCallbackWordlist.onSuccessResponse(wordList);

                        } catch (Exception e) {
                            System.out.println("Error in processing JSONresponse: " + e.getMessage());
                        }

                    }
                },
                //argument #4:
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        System.out.println("Error in webservice call: " + error.getMessage());
                    }
                });

        requestQueue.add(jsonArrayReq);
    }

    public void requestJsonWord(final Word currentWord, String urlextension) {
        String finalurl = url + urlextension;

        JsonObjectRequest jsonReq = new JsonObjectRequest(Request.Method.GET, finalurl, null,

                //stringrequest argument #3:
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {

                        //instantiating with id = -1... a bit messy maybe...
                        String wordText = "";
                        Long id = Long.valueOf(-1);
                        try {

                            wordText = response.getString("text");
                            id = response.getLong("id");
                        } catch (Exception e) {
                            e.getMessage();
                        }

                        //Todo: Change so that list of words are retrieved when starting game.
                        currentWord.setWordtext(wordText);
                        currentWord.setId(id);
                    }
                },

                //argument #4:
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        System.out.println("error occurred: " + error.toString());
                    }
                });

        requestQueue.add(jsonReq);
    }

    public void requestCategories(String urlExtension, final VolleyCallbackCategories volleyCallbackCategories) {
        String finalUrl = url + urlExtension;

        JsonArrayRequest jsonArrayReq = new JsonArrayRequest(Request.Method.GET, finalUrl, null,
                //stringrequest argument #3:
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {

                        JSONObject jsonObject;
                        try {
                            List<Category> categories = new ArrayList<Category>();
                            for (int i = 0; i < response.length(); i++) {
                                jsonObject = response.getJSONObject(i);
                                Integer id = jsonObject.getInt("id");
                                String categoryText = jsonObject.getString("categoryText");
                                categories.add(new Category(id, categoryText));
                            }
                            //System.out.println(categories.get(0));
                            volleyCallbackCategories.onSuccessResponse(categories);

                        } catch (Exception e) {
                            e.getMessage();
                        }
                        //Todo: Change so that list of words are retrieved when starting game.
                    }
                },
                //argument #4:
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        System.out.println("Error");
                    }
                });

        requestQueue.add(jsonArrayReq);
    }

    public void requestMoreWords(final WordList wordList, String urlextension) {
        String finalUrl = url + urlextension;

        //the jsonReq object is just a specification for what is to happen when queue.add is called!
        //nothing of the body of jsonReq is called until queue.add.
        JsonArrayRequest jsonArrayReq = new JsonArrayRequest(Request.Method.GET, finalUrl, null,
                //stringrequest argument #3:
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        JSONObject jsonObject;

                        try {
                            for (int i = 0; i < response.length(); i++) {
                                jsonObject = response.getJSONObject(i);
                                Long id = jsonObject.getLong("id");
                                String wordtext = jsonObject.getString("text");
                                wordList.addSingleWord(new Word(id, wordtext));
                            }
                            Log.i("WEB", "Replenished wordlist");

                        } catch (Exception e) {
                            Log.e("WEB", "Error in processing JSONresponse for replenishment: " + e.getMessage());
                        }
                    }
                },
                //argument #4:
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.e("WEB", "Error in call to replenish wordlist: " + error.getMessage());
                    }
                });

        requestQueue.add(jsonArrayReq);
    }

    //update existing word, but will maybe work to post new words as well.
    public void postWord(String urlExtension, Word word) {
        String finalUrl = url + urlExtension;

        //build JSONobject:
        JSONObject wordJson = new JSONObject();
        try {
            wordJson.put("id", word.getId());
            wordJson.put("text", word.getWordtext());
            wordJson.put("delflg", word.getDelflg());

        } catch (JSONException e) {
            e.printStackTrace();
        }

        JsonRequest jsonReq = new JsonObjectRequest(Request.Method.POST, finalUrl, wordJson,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {

                        } catch (Exception e) {
                            e.getMessage();
                        }
                    }
                },
                //argument #4:
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        System.out.println("error occurred: " + error.toString());
                    }
                });
        requestQueue.add(jsonReq);
    }

    public void postAssignWordsToCategory(String urlExtension, Category category, Word word) {
        String finalUrl = url + urlExtension;

        //build JSONobject:
        JSONObject completeJson = new JSONObject();
        JSONObject wordJson = new JSONObject();
        JSONObject categoryJson = new JSONObject();
        try {
            wordJson.put("id", word.getId());
            wordJson.put("text", word.getWordtext());

            categoryJson.put("id", category.getCategoryId());
            categoryJson.put("categoryText", category.getCategoryText());

            completeJson.put("word", wordJson);
            completeJson.put("category", categoryJson);

        } catch (JSONException e) {
            e.printStackTrace();
        }

        JsonRequest jsonReq = new JsonObjectRequest(Request.Method.POST, finalUrl, completeJson,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {

                        } catch (Exception e) {
                            e.getMessage();
                        }
                    }
                },
                //argument #4:
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        System.out.println("error occurred: " + error.toString());
                    }
                });

        requestQueue.add(jsonReq);
    }

}