package com.noddysoft.wordlyandroid.classes;

import java.io.Serializable;

public class Category implements Serializable {
    private Integer id;
    private String categoryText;


    public Category (Integer id, String categoryText) {
        this.id = id;
        this.categoryText = categoryText;
    }

    public Integer getCategoryId() {
        return id;
    }

    public String getCategoryText() {
        return categoryText;
    }

    public void setCategoryId(Integer categoryId) {
        this.id = categoryId;
    }

    public void setCategoryText(String categoryText) {
        this.categoryText = categoryText;
    }

    //toString-method is used to display text in Spinner
    @Override
    public String toString() {
        return categoryText;
    }
}
