package com.noddysoft.wordlyandroid.classes;

import android.app.Application;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class WordList extends Application implements Serializable {
    //made the class serializable in order to make it usable with intent.extra
    private static WordList instance;
    private List<Word> wordlist;

    private WordList(){
        wordlist = new ArrayList<Word>();
    }

    public void setWordlist(List<Word> wordlist) {
        this.wordlist = wordlist;
    }

    public static WordList getInstance(){
        if (instance == null) {
            instance = new WordList();
        }
        return instance;
    }

    public List<Word> getWordlist() {
        return wordlist;
    }

    public void addSingleWord(Word word){
        wordlist.add(word);
    }

    public Integer getLength() {
        return wordlist.size();
    }

    //returns the first Word in the list, and then removes it. Returns null if there is nothing in the list.
    public Word popWord(){
        if (!wordlist.isEmpty()){
            Word word = wordlist.get(0);
            wordlist.remove(0);
            return word;
        }
        return null;
    }

    public void clear() {
        wordlist.clear();
    }
}
