package com.noddysoft.wordlyandroid.classes;

import java.util.HashMap;
import java.util.List;

public interface VolleyCallbackCategories {
    //void onSuccessResponse(String wordtext);
    void onSuccessResponse(List<Category> categories);
}
