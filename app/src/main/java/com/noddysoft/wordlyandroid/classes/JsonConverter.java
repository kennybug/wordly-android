package com.noddysoft.wordlyandroid.classes;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Erik on 26.09.2018.
 */

public class JsonConverter {

    public static String unwrapWordText(JSONObject jsonObject) {
        String wordText = "";
        try {
            wordText = jsonObject.getString("text");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return wordText;
    }

}
