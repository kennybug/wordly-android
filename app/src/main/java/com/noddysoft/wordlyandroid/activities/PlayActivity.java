package com.noddysoft.wordlyandroid.activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.webkit.WebView;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.noddysoft.wordlyandroid.R;
import com.noddysoft.wordlyandroid.classes.Category;
import com.noddysoft.wordlyandroid.classes.GameSession;
import com.noddysoft.wordlyandroid.classes.VolleyCallback;
import com.noddysoft.wordlyandroid.classes.VolleyCallbackCategories;
import com.noddysoft.wordlyandroid.classes.VolleyCallbackWordlist;
import com.noddysoft.wordlyandroid.classes.WebserviceConnector;
import com.noddysoft.wordlyandroid.classes.Word;
import com.noddysoft.wordlyandroid.classes.WordList;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class PlayActivity extends AppCompatActivity {

    private WebserviceConnector webconn;
    private Word currentWord;
    private WordList sessionWordlist;
    private Category selectedCategory;
    private GameSession gameSession;
    private Spinner categorySpinner;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_play);
        webconn = WebserviceConnector.getInstance(this, getString(R.string.random));
        gameSession = GameSession.getInstance();
        selectedCategory = gameSession.getSelectedCategory();
        setupCategorySpinner();

        sessionWordlist = WordList.getInstance();

        //execute first extraction of word.
        getWord();

        //TODO: If you check "local" then WordsToPlay will read from the local .txt where all words are stored as jsonfiles. If you play online then it will read from the webservice-returned list of jsonwords.
    }

    private void setupCategorySpinner() {
        categorySpinner = (Spinner)findViewById(R.id.assign_category_spinner);

        //Filter categorylist to not edit standard categories.
        final List<Category> userAssignableCategories = returnFilteredCategories(gameSession.getAvailableCategories());

        ArrayAdapter<Category> categoryAdapter = new ArrayAdapter<Category>(getApplicationContext(),
                android.R.layout.simple_spinner_dropdown_item, userAssignableCategories);
        categorySpinner.setAdapter(categoryAdapter);
        categorySpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                selectedCategory = userAssignableCategories.get(i);
            }
            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
                //not really used - have a null-category as 0 index
            }
        });
    }

    private List<Category> returnFilteredCategories(List<Category> categories) {
        List<Category> filteredCategories = new ArrayList<>();

        //TODO: put protected categories in resources instead of hardcoding.
        String[] protectedCategoryTexts = new String[] {"Substantiv", "Verb", "Adjektiv"};

        for (Category category : categories) {
            //only add categories *not* in the protected list
            if (!Arrays.asList(protectedCategoryTexts).contains(category.getCategoryText())) {
                filteredCategories.add(category);
             }
        }
        return filteredCategories;
    }

    //Retrieve word from the sessionwordlist when user presses "get word" button
    public void getWordClick(View view) {
        getWord();
    }

    public void getWord() {
        updateWordlistIfRunningShort();
        sendCategoryIfAssigned();

        currentWord = sessionWordlist.popWord();
        if (currentWord != null) {
            setDisplayedText(currentWord.getWordtext());
            categorySpinner.setSelection(0);
        }
        else {
            Log.i("PLAYACT","No words remaining in the session wordlist");
        }
    }

    public void setDisplayedText(String text) {
        TextView wordTextView = (TextView) findViewById(R.id.wordTextView);
        wordTextView.setText(text);
    }

    //TODO: Implement method to add to favorites
    public void addToFavorites() {
    }

    private void updateWordlistIfRunningShort() {
        if (sessionWordlist.getLength() <= 5) {
            String urlExtension;
            if (gameSession.getSelectedCategory().getCategoryId() == null) {
                urlExtension = "subset";
            } else {
                urlExtension = "subsetcat?catId=" + gameSession.getSelectedCategory().getCategoryId().toString();
            }
            webconn.requestMoreWords(WordList.getInstance(), urlExtension);
        }
    }

    private void sendCategoryIfAssigned() {
        if (categorySpinner.getSelectedItemPosition() == 0 ) {

        } else {
            webconn.postAssignWordsToCategory("map", selectedCategory, currentWord);
            Toast.makeText(
                    getBaseContext(),
                    currentWord.getWordtext() + " tilordnet kategori " + selectedCategory.getCategoryText(),
                    Toast.LENGTH_SHORT
            ).show();
        }
    }

    public void flagBadWord(View view) {
        if (currentWord != null) {
            currentWord.setDelflg(true);
            webconn.postWord("setWord", currentWord);
            Toast.makeText(
                    getBaseContext(),
                    "Ordet " + currentWord.getWordtext() + " ble rapportert.",
                    Toast.LENGTH_SHORT
            ).show();
        }
    }

    public void lookUpWordDefinition(View view) {
        if (currentWord != null) {
            Intent playIntent = new Intent(PlayActivity.this, WebviewActivity.class);
            playIntent.putExtra("wordtext", currentWord.getWordtext());
            startActivity(playIntent);

//            webview = new WebView(this);
//            setContentView(webview);
//            String lookupUrl = "https://ordbok.uib.no/perl/ordbok.cgi?OPP=br%C3%B8d&ant_bokmaal=5&ant_nynorsk=5&begge=+&ordbok=begge";    // buildWordDefinitionUrl();
//
//            webview.loadUrl(lookupUrl);
//            webview.requestFocus();

        }
    }

//    private String buildWordDefinitionUrl() {
//
//    }
}
