package com.noddysoft.wordlyandroid.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.toolbox.Volley;
import com.noddysoft.wordlyandroid.R;
import com.noddysoft.wordlyandroid.classes.Category;
import com.noddysoft.wordlyandroid.classes.GameSession;
import com.noddysoft.wordlyandroid.classes.VolleyCallback;
import com.noddysoft.wordlyandroid.classes.VolleyCallbackCategories;
import com.noddysoft.wordlyandroid.classes.VolleyCallbackWordlist;
import com.noddysoft.wordlyandroid.classes.WebserviceConnector;
import com.noddysoft.wordlyandroid.classes.Word;
import com.noddysoft.wordlyandroid.classes.WordList;

import org.json.JSONException;

import java.util.HashMap;
import java.util.List;

public class FrontPageActivity extends AppCompatActivity {

    private Spinner categorySpinner;
    private ArrayAdapter<CharSequence> categoryAdapter;
    private List<Category> playableCategories;
    private Category selectedCategory;
    private WebserviceConnector webconn;
    private GameSession gameSession;
    private int playMode;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_front_page);

        gameSession = GameSession.getInstance();
        webconn = WebserviceConnector.getInstance(this.getApplication(), getString(R.string.random));

        //Instantiate spinner on startup
        setupCategorySpinner();
    }

    private void setupCategorySpinner() {
        categorySpinner = (Spinner) findViewById(R.id.category_spinner);

        webconn.requestCategories(this.getString(R.string.categories), new VolleyCallbackCategories() {
            @Override
            public void onSuccessResponse(List<Category> categories) {
                categories.add(0, new Category(null, ""));
                //categoryList = categories;
                gameSession.setAvailableCategories(categories);
                playableCategories = categories;

                ArrayAdapter<Category> categoryAdapter = new ArrayAdapter<Category>(getApplicationContext(),
                        android.R.layout.simple_spinner_dropdown_item, playableCategories);
                categorySpinner.setAdapter(categoryAdapter);
                categorySpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                        selectedCategory = playableCategories.get(i);
                        gameSession.setSelectedCategory(selectedCategory);
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> adapterView) {
                        //not really used - have a null-category as 0 index
                    }
                });
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_front_page, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    //PressPlayButton - Runs webservice to get words, and then changes activity when words have been downloaded:
    public void pressPlayButton(View view) {
        //clear wordlist to allow for new selection
        WordList.getInstance().clear();

        String urlExtension;
        if (selectedCategory.getCategoryId() == null) {
            urlExtension = "subset";
        } else {
            urlExtension = "subsetcat?catId=" + selectedCategory.getCategoryId().toString();
        }
        extractWordsAndStartActivity(urlExtension);
    }

    private void extractWordsAndStartActivity(String urlExtension) {
        webconn.requestWordlist(WordList.getInstance(), urlExtension, new VolleyCallbackWordlist() {
            @Override
            public void onSuccessResponse(WordList wordlist) {
                try {
                    executeIntent();

                } catch (Exception e) {
                    System.out.println("Error: " + e.getMessage());
                    //TODO: should send message to user - not able to get connection?
                }
            }
        });
    }

    private void executeIntent() {
        if (playMode == 1) {
            Intent playIntent = new Intent(FrontPageActivity.this, PlayActivity.class);
            startActivity(playIntent);
        } else {
            readCountDownTimerIntoGameSession();
            Intent playIntent = new Intent(FrontPageActivity.this, PlayGuessActivity.class);
            startActivity(playIntent);
        }
    }

    public void setPlayModeGuess(View view) {
        playMode = 0;
        toggleTimerVisible(true);
    }

    public void setPlayModeFree(View view) {
        playMode = 1;
        toggleTimerVisible(false);
    }

    private void toggleTimerVisible(Boolean visible) {
        View timerLayout = findViewById(R.id.timerLayout);
        if (visible == true) {
            timerLayout.setVisibility(View.VISIBLE);
            EditText timerEditText = findViewById(R.id.timerEditText);
            timerEditText.setText(gameSession.getCountDownTimeInSecondsAsString());

        } else {
            timerLayout.setVisibility(View.INVISIBLE);
        }
    }

    private void readCountDownTimerIntoGameSession() {
        EditText timerEditText = findViewById(R.id.timerEditText);
        if (!timerEditText.getText().toString().equals("")) {
            Long milliseconds = Long.valueOf(timerEditText.getText().toString()) * 1000;
            gameSession.setCountDownTime(milliseconds);
        }
    }


}
