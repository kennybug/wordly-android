package com.noddysoft.wordlyandroid.activities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.webkit.WebView;

import com.noddysoft.wordlyandroid.R;

public class WebviewActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_webview);
        String wordtext = getIntent().getStringExtra("wordtext");


        WebView webview = new WebView(this);
        setContentView(webview);
        String lookupUrl = buildWebviewUrl(wordtext);    // buildWordDefinitionUrl();

        webview.loadUrl(lookupUrl);
        webview.requestFocus();
    }

    private String buildWebviewUrl(String wordtext) {
        //String url = "https://ordbok.uib.no/perl/ordbok.cgi?OPP=" + wordtext + "&ant_bokmaal=5&ant_nynorsk=5&begge=+&ordbok=begge";
        String[] url = getResources().getStringArray(R.array.wordDefinitionUrl);
        String finalUrl = url[0] + wordtext + url[1];
        return finalUrl;
    }
}
