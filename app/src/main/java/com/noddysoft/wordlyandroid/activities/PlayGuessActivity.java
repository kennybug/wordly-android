package com.noddysoft.wordlyandroid.activities;

import android.app.Application;
import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.noddysoft.wordlyandroid.R;
import com.noddysoft.wordlyandroid.classes.AudioPlayer;
import com.noddysoft.wordlyandroid.classes.Category;
import com.noddysoft.wordlyandroid.classes.GameSession;
import com.noddysoft.wordlyandroid.classes.WebserviceConnector;
import com.noddysoft.wordlyandroid.classes.Word;
import com.noddysoft.wordlyandroid.classes.WordList;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class PlayGuessActivity extends AppCompatActivity {

    private WebserviceConnector webconn;
    private Word currentWord;
    private WordList sessionWordlist;
    private Category selectedCategory;
    private GameSession gameSession;
    private Spinner categorySpinner;
    TextView timerTextView;
    //Declare timer
    CountDownTimer cTimer = null;
    Button timerButton;
    Integer points = 0;
    AudioPlayer audioPlayer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_play_guess);
        webconn = WebserviceConnector.getInstance(this, getString(R.string.random));
        gameSession = GameSession.getInstance();
        selectedCategory = gameSession.getSelectedCategory();
//        setupCategorySpinner();

        sessionWordlist = WordList.getInstance();

        //TODO: If you check "local" then WordsToPlay will read from the local .txt where all words are stored as jsonfiles. If you play online then it will read from the webservice-returned list of jsonwords.
        timerButton = (Button) findViewById(R.id.timerButton);
        timerTextView = findViewById(R.id.timerTextView);

        timerTextView.setText(gameSession.getCountDownTimeInSecondsAsString());
        setDisplayedPoints(0);

        audioPlayer = new AudioPlayer();
    }

    //start timer function
    public void startTimer(View view) {
        if (cTimer == null) {
//            //play start sound
//            audioPlayer.play(getApplicationContext(), R.raw.wordlysoundbites_start);
            //update timerplaybutton to show stop
            timerButton.setText(R.string.timerStop);
            //extract a word immediately when starting the clock:
            getWord();

            cTimer = new CountDownTimer(gameSession.getCountDownTimeInMilliseconds(), 1000) {
                public void onTick(long millisUntilFinished) {
                      Long secondsUntilFinished = millisUntilFinished / 1000;
                      timerTextView.setText(secondsUntilFinished.toString());


                }

                public void onFinish() {
                    audioPlayer.play(getApplicationContext(), R.raw.wordlysoundbites_timeout);
                    timerTextView.setText(getString(R.string.timeout));
                    cTimer = null;
                    timerButton.setText(R.string.timerStart);
                }
            };
            cTimer.start();
        } else {
            //cancel and destroy cTimer object.

            cTimer.cancel();
            cTimer = null;
             timerButton.setText(getString(R.string.timerStart));
             timerTextView.setText(gameSession.getCountDownTimeInSecondsAsString());
        }
    }

    //cancel timer
    public void cancelTimer() {
        if(cTimer!=null)
            cTimer.cancel();
    }

//    private void setupCategorySpinner() {
//        categorySpinner = (Spinner)findViewById(R.id.assign_category_spinner);
//
//        //Filter categorylist to not edit standard categories.
//        final List<Category> userAssignableCategories = returnFilteredCategories(gameSession.getAvailableCategories());
//
//        ArrayAdapter<Category> categoryAdapter = new ArrayAdapter<Category>(getApplicationContext(),
//                android.R.layout.simple_spinner_dropdown_item, userAssignableCategories);
//        categorySpinner.setAdapter(categoryAdapter);
//        categorySpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
//            @Override
//            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
//                selectedCategory = userAssignableCategories.get(i);
//            }
//            @Override
//            public void onNothingSelected(AdapterView<?> adapterView) {
//                //not really used - have a null-category as 0 index
//            }
//        });
//    }

//    private List<Category> returnFilteredCategories(List<Category> categories) {
//        List<Category> filteredCategories = new ArrayList<>();
//
//        //TODO: put protected categories in resources instead of hardcoding.
//        String[] protectedCategoryTexts = new String[] {"Substantiv", "Verb", "Adjektiv"};
//
//        for (Category category : categories) {
//            //only add categories *not* in the protected list
//            if (!Arrays.asList(protectedCategoryTexts).contains(category.getCategoryText())) {
//                filteredCategories.add(category);
//             }
//        }
//        return filteredCategories;
//    }

    public void skipWordClick(View view) {
        getWord();
    }

    public void correctGuessClick(View view) {
        audioPlayer.play(this, R.raw.wordlysoundbites_point);
        addPoint();
        getWord();
    }


    //Retrieve word from the sessionwordlist when user presses "get word" button
    public void getWord() {
        updateWordlistIfRunningShort();
//        sendCategoryIfAssigned();

        currentWord = sessionWordlist.popWord();
        if (currentWord != null) {
            setDisplayedText(currentWord.getWordtext());
//            categorySpinner.setSelection(0);
        }
        else {
            Log.i("PLAYACT","No words remaining in the session wordlist");
        }
    }

    public void setDisplayedText(String text) {
        TextView wordTextView = (TextView) findViewById(R.id.wordTextView);
        wordTextView.setText(text);
    }

    //TODO: Implement method to add to favorites
    public void addToFavorites() {
    }

    private void updateWordlistIfRunningShort() {
        if (sessionWordlist.getLength() <= 5) {
            String urlExtension;
            if (gameSession.getSelectedCategory().getCategoryId() == null) {
                urlExtension = "subset";
            } else {
                urlExtension = "subsetcat?catId=" + gameSession.getSelectedCategory().getCategoryId().toString();
            }
            webconn.requestMoreWords(WordList.getInstance(), urlExtension);
        }
    }

//    private void sendCategoryIfAssigned() {
//        if (categorySpinner.getSelectedItemPosition() == 0 ) {
//
//        } else {
//            webconn.postAssignWordsToCategory("map", selectedCategory, currentWord);
//            Toast.makeText(
//                    getBaseContext(),
//                    currentWord.getWordtext() + " tilordnet kategori " + selectedCategory.getCategoryText(),
//                    Toast.LENGTH_SHORT
//            ).show();
//        }
//    }

//    public void flagBadWord(View view) {
//        if (currentWord != null) {
//            currentWord.setDelflg(true);
//            webconn.postWord("setWord", currentWord);
//        }
//    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        cancelTimer();

    }

    public void resetPoints(View view) {
        this.points = 0;
        setDisplayedPoints(this.points);
    }

    private void addPoint() {
        this.points += 1;
        setDisplayedPoints(this.points);
    }

    private void setDisplayedPoints(Integer points){
        TextView pointView = findViewById(R.id.pointsTextView);
        pointView.setText(points.toString());
    }
}
